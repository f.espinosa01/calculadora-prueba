package com.example.demo.CalculadoraTest;

import com.example.demo.models.CalculadoraModel;

import static org.junit.jupiter.api.Assertions.*;

import com.example.demo.services.CalculadoraService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculadoraModelTest {
    CalculadoraModel calculadora;
    @Autowired
    CalculadoraService calculadoraService;

    @BeforeEach
    void setUp() {
        calculadora = new CalculadoraModel(0, 4, 2);
    }

    @Test
    void verificarSuma() {
        int esperado = 6;
        assertEquals(esperado, calculadoraService.sumar(calculadora));
    }

    @Test
    void verificarResta() {
        int esperado = 2;
        assertEquals(esperado, calculadoraService.restar(calculadora));
    }

    @Test
    void verificarMultiplicacion() {
        int esperado = 8;
        assertEquals(esperado, calculadoraService.multiplicar(calculadora));
    }

    @Test
    void verificarDivision() {
        int esperado = 2;
        assertEquals(esperado, calculadoraService.dividir(calculadora));
    }

    @AfterEach
    void tearDown() {
        calculadora = null;
    }
}
