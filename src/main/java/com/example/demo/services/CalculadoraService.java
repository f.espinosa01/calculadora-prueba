package com.example.demo.services;

import com.example.demo.dao.CalculadoraRepository;
import com.example.demo.models.CalculadoraModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {
    @Autowired
    CalculadoraRepository calculadoraRepository;

    public CalculadoraModel guardarCalculadora(CalculadoraModel calculadoraModel) {
        return calculadoraRepository.save(calculadoraModel);
    }

    public int sumar(CalculadoraModel calculadora) {
        return calculadora.getNum1() + calculadora.getNum2();
    }

    public int restar(CalculadoraModel calculadora) {
        return calculadora.getNum1() - calculadora.getNum2();
    }

    public int multiplicar(CalculadoraModel calculadora) {
        return calculadora.getNum1() * calculadora.getNum2();
    }

    public int dividir(CalculadoraModel calculadora) {
        return calculadora.getNum1() / calculadora.getNum2();
    }
}
