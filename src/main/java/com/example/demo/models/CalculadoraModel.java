package com.example.demo.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculadoraModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private int num1;
    private int num2;

    public CalculadoraModel(CalculadoraRequestModel calculadora) {
        this.id = calculadora.getId();
        this.num1 = calculadora.getNum1();
        this.num2 = calculadora.getNum2();
    }

}
