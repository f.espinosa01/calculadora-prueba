package com.example.demo.controllers;

import com.example.demo.dao.CalculadoraRepository;
import com.example.demo.models.CalculadoraModel;
import com.example.demo.models.CalculadoraRequestModel;
import com.example.demo.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("calculadora")
public class CalculadoraController {
    @Autowired
    CalculadoraService calculadoraService;
    @Autowired
    CalculadoraRepository calculadoraRepository;

    @PostMapping("/guardar")
    public CalculadoraModel guardarCalculadora(@RequestBody CalculadoraRequestModel calculadora) {
        CalculadoraModel calculadoraModel = new CalculadoraModel(calculadora);
        return calculadoraService.guardarCalculadora(calculadoraModel);
    }

    @GetMapping("/sumar-{id}")
    public int sumarCalculadora(@PathVariable int id) {
        return calculadoraService.sumar(calculadoraRepository.getReferenceById(id));
    }

    @GetMapping("/restar-{id}")
    public int restarCalculadora(@PathVariable int id) {
        return calculadoraService.restar(calculadoraRepository.getReferenceById(id));
    }

    @GetMapping("/multiplicar-{id}")
    public int multiplicarCalculadora(@PathVariable int id) {
        return calculadoraService.multiplicar(calculadoraRepository.getReferenceById(id));
    }

    @GetMapping("/dividir-{id}")
    public int dividirCalculadora(@PathVariable int id) {
        return calculadoraService.dividir(calculadoraRepository.getReferenceById(id));
    }

}
