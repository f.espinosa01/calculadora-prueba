package com.example.demo.dao;

import com.example.demo.models.CalculadoraModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CalculadoraRepository extends JpaRepository<CalculadoraModel, Integer> {

}
